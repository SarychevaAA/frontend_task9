function showMyDB(personalMovieDB){
    if (personalMovieDB.privat === false){
        console.log(personalMovieDB);
    }
}
function writeYourGenres(personalMovieDB){
    let genreQuestions = 0;
    while (genreQuestions < 3){
        let loveGenre = prompt( `Ваш любимый жанр под номером ${genreQuestions+1}`, '');
        personalMovieDB.genres.push(loveGenre);
        genreQuestions++;
    }
}
let numberOfFilms = prompt('Сколько фильмов вы уже посмотрели?', '0');
const personalMovieDB = {
    count: numberOfFilms,
    movies: {},
    actors: {},
    genres: [],
    privat: false
}

if (numberOfFilms < 10 && numberOfFilms >= 0){
    alert('Просмотрено довольно мало фильмов');
}
else if (numberOfFilms <= 30 && numberOfFilms >= 0){
    alert('Вы классический зритель');
}
else if (numberOfFilms > 30){
    alert('Вы киноман');
}
else{
    alert('Произошла ошибка');
}

let countQuestions = 0
while (countQuestions < 2){
    let lastSeeFilm = prompt( 'Один из последних просмотренных фильмов?', '')
    let filmRating = prompt('На сколько оцените его?', '')
    if (lastSeeFilm.length < 50){
        alert('Название фильма короче 50 символов, попробуйте еще раз');
    }
    else{
        personalMovieDB.movies[lastSeeFilm] = filmRating;
        countQuestions++;
    }
}
showMyDB(personalMovieDB);
writeYourGenres(personalMovieDB);
